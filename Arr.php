<?php

class Arr
{
    /**
     * Find first position of minimum.
     *
     * @param  array $array
     * @return int|null
     */
    private static function firstMinPosition(array $array): ?int
    {
        $position = null;

        foreach ($array as $key => $value) {
            if (
                $position === null ||
                $array[$position] > $value
            ) {
                $position = $key;
            }
        }

        return $position;
    }

    /**
     * Find first number near needle number.
     *
     * @param  int $needle
     * @param  int[] $haystack
     * @return int|null
     */
    public static function nearNumber(int $needle, array $haystack): ?int
    {
        $diff = [];
        foreach ($haystack as $key => $value) {
            $diff[$key] = abs($needle - $value);
        }

        return self::firstMinPosition($diff);
    }

    /**
     * Find first number near needle number for sortable array.
     *
     * @param  int $needle
     * @param  int[] $haystack
     * @return int|null
     */
    public static function nearNumberForSorted(int $needle, array $haystack): ?int
    {
        $leftBorder  = 0;
        $rightBorder = count($haystack) - 1;

        // Check left position.
        if ($needle <= $haystack[$leftBorder]) {
            return $leftBorder;
        }
        // Check right border.
        if ($needle >= $haystack[$rightBorder]) {
            return $rightBorder;
        }

        // Calc first position.
        $position = round(($rightBorder - $leftBorder) / 2);

        while (true) {
            // Check equal current position value.
            $current = $haystack[$position];
            if ($needle === $current) {
                break;
            }

            // Calculate near position
            if ($needle > $current) {
                $nearPosition = $position + 1;

                // Check for crossing the right border.
                if ($nearPosition > $rightBorder) {
                    break;
                }
            } else {
                $nearPosition = $position - 1;

                // Check for crossing the right border.
                if ($nearPosition < $leftBorder) {
                    break;
                }
            }

            // Receive near value and check if needle is between the current and next value.
            $next = $haystack[$nearPosition];
            if ($next >= $needle && $current < $needle) {
                // Check which of values is closer to needle.
                if (abs($next - $needle) < abs($current - $needle)) {
                    $position = $nearPosition;
                }
                break;
            }

            // Shifting the border. Left if the current value is less. Right if the current value is greater.
            if ($needle > $current) {
                $leftBorder  = $position;
            } else {
                $rightBorder = $position;
            }

            // Calculate next position.
            $position = $leftBorder + round(($rightBorder - $leftBorder) / 2);
        }

        return $position;
    }
}
