CREATE TABLE `user`
(
    `id`     int(11),
    `name`   varchar(255),
    `gender` tinyint(4),
    `dep_id` int(11)
);

INSERT INTO `user`
VALUES ( 1, 'Wehner',     2, 5),
       ( 2, 'Bins',       1, 4),
       ( 3, 'Ullrich',    1, 5),
       ( 4, 'Zulauf',     2, 4),
       ( 5, 'Stark',      1, 2),
       ( 6, 'Kub',        2, 3),
       ( 7, 'Stark',      2, 3),
       ( 8, 'Keebler',    1, 1),
       ( 9, 'Hodkiewicz', 1, 5),
       (10, 'Casper',     2, 4),
       (11, 'Kautzer',    1, 5),
       (12, 'Stamm',      2, 1),
       (13, 'Bode',       2, 2),
       (14, 'Durgan',     2, 1),
       (15, 'Beatty',     1, 4),
       (16, 'Becker',     1, 1),
       (17, 'Dietrich',   2, 3),
       (18, 'Dickens',    1, 1),
       (19, 'Shields',    2, 5),
       (20, 'Welch',      1, 3);

SELECT `dep_id`,
       COUNT(IF(`gender` = 1, 1, NULL)) as count_man,
       COUNT(IF(`gender` = 2, 1, NULL)) as count_women
FROM `user`
GROUP BY `dep_id`
ORDER BY `dep_id` ASC;

------------------------------------------------------------

SELECT `dep_id`,
       SUM(2 - `gender`) as count_man,
       SUM(`gender` - 1) as count_women
FROM `user`
GROUP BY `dep_id`
ORDER BY `dep_id` ASC;
